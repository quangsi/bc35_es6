// var function scope ~ bị giới hạn bởi function

function checkLogin() {
  console.log("thongBao: ", thongBao);

  var isLogin = true;
  if (isLogin) {
    var thongBao = "Đăng nhập thành công";
  } else {
    var thongBao = "Đăng nhập thất bại";
  }
}
checkLogin();

// let block scope {}

function check_login() {
  let isLogin = true;
  if (isLogin) {
    let thongBao = "Thành công";
  } else {
    let thongBao = "Thất bại";
  }
  console.log("thongBao: ", thongBao);
}
