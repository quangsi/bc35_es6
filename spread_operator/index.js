// object

let alice = {
  score: 10,
  // child: {
  //   score: 11,
  // },
};
Destructuring;
// spread operator : copy, update, add

let bob = { ...alice, score: 2, address: "VN" }; //shalow copy

// deep copy=> json.stringtify
// bob.score = 2;

console.log("bob: ", bob);
console.log("alice: ", alice);

// array
let colors = ["red", "green", "blue"];
let newColor = [...colors, "black", "pink"];
newColor.push(`danh sách: ${colors}`);
