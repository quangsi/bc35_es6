// function sayHello() {
//   console.log("Hello user");
// }

// let sayHello = function(username) {
//   console.log(`Hello ${username}`);
// };

let sayHello = (username) => {
  console.log(`Hello ${username}`);
};

sayHello("Alice");

// let doubleMoney=()=>{

// }
// let doubleMoney = function (number) {
//   return number * 2;
// };

let doubleMoney = (number) => number * 2;
