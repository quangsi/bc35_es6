let tinhDTB = (toan, ly, hoa = 0) => {
  return (toan + ly + hoa) / 3;
};

let sv1 = tinhDTB(3, 3, 3);
console.log("sv1: ", sv1);

let sv2 = tinhDTB(3, 3);
console.log("sv2: ", sv2);

// NaN not a number
