let sv = {
  // infor: {
  // },

  name: "alice",
  age: 2,
};
let { name, age } = sv;
console.log("age: ", age);
console.log("name: ", name);
// console.log(sv.infor.name);

let colors = ["red", "blue"];

function useColor() {
  return colors;
}
var [c1, c2] = useColor();
console.log("c2: ", c2);
console.log("c1: ", c1);
